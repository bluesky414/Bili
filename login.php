<?php

require_once 'index.php';
$act = isset($_REQUEST['act'])?trim($_REQUEST['act']):'';

//实例化B站对象类
$cookie = dirname(__FILE__).'/bCookie.tmp';
$imgUrl = "http://account.bilibili.com/captcha";							//B站登录验证码地址   方法get
$dologinUrl = "http://account.bilibili.com/login/dologin";					//处理提交登录信息地址  方法post
$indexUrl = "http://account.bilibili.com";									//用户中心首页
$sendMsgUrl = 'http://live.bilibili.com/msg/send';							//处理B站直播弹幕数据地址 方法post
$getStatusUrl = 'http://live.bilibili.com/live/getInfo?roomid=52468';		//B站直播间基本信息  方法GET
$changeStatusUrl = 'http://live.bilibili.com/liveact/live_status_mng/';		//更改直播间开启和关闭状态  方法post
$msgUrl = 'http://live.bilibili.com/ajax/msg';								//直播聊天记录信息    方法post
$bili = new Bilibili();
switch($act){
	case 'showauth':	//显示验证码
		header('Content-type:image/png');
		echo $bili->getCheckimg($imgUrl, $cookie);
		break;
	case 'login':		//模拟登录
		$html = '<iframe src="?act=showauth" width="130" height="60" border="0"></iframe>';
		$html .= '<form><input type="hidden" name="act" value="dologin"><input type="text" name="code"><input type="submit" value="登录"></form>';
		echo $html;
		break;
	case 'dologin':
		$code = isset($_REQUEST['code'])?trim($_REQUEST['code']):'';
		if($code == ''){
			$code = '-300';
			$msg = '请填入验证码';
			$result = array('code'=>$code,'msg'=>$msg);
			echo json_encode($result);
			return false;	
		}
		//构造登录表单数据
		$loginData = array(
			'act'=>'login',
			'gourl'=>'http://account.bilibili.com/',
			'keeptime'=>'604800',
			'userid'=>'xxxxxx',
			'pwd'=>'xxxxxxxx',
			'vdcode'=>$code,
			'keeptime'=>'604800'
			);
		$result = $bili->dologin($dologinUrl, $cookie, $loginData);
		echo $result;
		break;
	case  'chat':		//直播提交弹幕
		$html = '<form><input type="hidden" name="act" value="chat"><input type="text" name="text"><input type="submit" value="发表"></form>';
		echo $html;
		$text = isset($_REQUEST['text'])?$_REQUEST['text']:'';
		$dataArr = array('color'=>'16777215','fontsize'=>'30','mode'=>'1','msg'=>$text,'rnd'=>'1452337852','roomid'=>52468);
		$bili->http_request($sendMsgUrl, $cookie, $dataArr);
		break;
	case 'live_status':
		$status = isset($_REQUEST['status'])?$_REQUEST['status']:'';
		$dataArr = array('status'=>$status,'roomid'=>'52468');
		$bili->http_request($changeStatusUrl, $cookie, $dataArr);
		$statusStr = json_decode($bili->http_request($getStatusUrl, $cookie));
		if($status == 1 && $statusStr->data->_status == 'on'){
			echo '开启成功！';	
		}elseif($status == 0 && $statusStr->data->_status == 'off'){
			echo '关闭成功！';	
		}else{
			echo '哎呀出错啦！请检查代码！';	
		}
		break;
	case 'get_liveMsg':
    	header('Content-type:text/html;charset=utf-8');
		//$time = $_COOKIE['lasttime'];
		$time = isset($_COOKIE['lasttime']) ? $_COOKIE['lasttime'] : $_POST['time'];
		if($time<$_POST['time']){
			$time = $_POST['time'];
		}
		//$time = strtotime($timeFormat);
		$dataArr = array('roomid'=>52468);
		$msgs_arr = json_decode($bili->http_request($msgUrl, $cookie, $dataArr),true);
		$msgs_arr = $msgs_arr['data']['room'];
		$msgarr = array();
		if(!empty($msgs_arr)){
			foreach($msgs_arr as $msg){
				$talktime = strtotime($msg['timeline'])*1000;
				if($talktime > $time){
					$msgarr[]=array('time'=>$msg['timeline'],'name'=>$msg['nickname'],'text'=>$msg['text']);
					$time = $talktime;	
				}
			}
			
		}
		if(empty($msgarr)){
				return;
		}else{
			$msgA = $msgarr;
			$arr = array_pop($msgA);
			setcookie('lasttime',strtotime($arr['time'])*1000);
			$msgarr = array('data'=>$msgarr);
			$msg = json_encode($msgarr);
			echo $msg;	
		}
		break;
	case 'getSilverImg':
		$silverImgUrl = 'http://live.bilibili.com/FreeSilver/getCaptcha?t='.$bili->randomFloat();
		$result = $bili->http_request($silverImgUrl, $cookie);
		header("Content-type:image/jpeg");
		echo $result;
		break;
	case 'inputAuth':
		$html = '<iframe src="?act=getSilverImg" width="130" height="60" border="0"></iframe>';
		$html .= '<form><input type="hidden" name="act" value="getSilver"><input type="text" name="code"><input type="submit" value="领取"></form>';
		echo $html;
		break;
 	case 'getSilver':
		$r = $bili->randomFloat();
		$captcha = isset($_REQUEST['code']) ? $_REQUEST['code'] : '';
		$silverUrl = 'http://live.bilibili.com/freeSilver/getAward?r='.$r.'&roomid=52468&captcha='.$captcha;
		$result = $bili->http_request($silverUrl, $cookie);
		echo $result;
        break;
	case 'liuli_reg':
		$regUrl = 'http://www.hacg.lol/wp/wp-login.php?action=register';
		$llCookie = dirname(__FILE__).'/liuli.tmp';
		$regData = array(
			'user_login'=>'wuyudeai123',
			'nickname'=>'wuyudeai123',
			'user_email'=>'575768423@qq.com',
			'pass1'=>'a123456',
			'pass2'=>'a123456',
			'description'=>'',
			'user_url'=>'',
			//'user_proof'=>'http://www.hacg.lol/wp',
			'action'=>'register',
			'log'=>'1111',
			'pwd'=>'1111',
			'googleopt'=>'11111',
			'wp_submit'=>'登录',
			'redirect_to'=>'http://www.hacg.lol/wp/wp-admin/',
			'testcookie'=>1
			);
		echo $bili->http_request($regUrl, $llCookie, $regData);
		break;
	default:
		$html = '<div>如果第一次进入本页面，请先点击登录</div>';
		$html .= '<ul>';
		$html .='<li><a href="?act=login">登录B站</a></li>';
		$html .='<li><form><input type="hidden" name="act" value="live_status"><input type="hidden" name="status" value="1"><input type="submit" value="开启"></form></li>';
		$html .='<li><form><input type="hidden" name="act" value="live_status"><input type="hidden" name="status" value="0"><input type="submit" value="关闭"></form></li>';
		$html .='<li><a href="?act=chat">发表直播弹幕</a></li>';
		$html .='<li><a href="?act=get_liveMsg">获取聊天记录</a></li>';
		$html .='<li><a href="?act=inputAuth">领取银瓜子</a></li>';
		$html .='<li><a href="?act=liuli_reg">琉璃注册</a></li>';
		$html .='</ul>';
		echo $html;
		break;
}