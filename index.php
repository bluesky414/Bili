<?php
//方法类
class Bilibili{ 	
	/**
	* @param url 请求路径
	* @param cookie cookie文件
	* @param data post数据，如果存在则为post，不存在则为get
	* @return result 返回值
	*/
	public function http_request($url, $cookie, $data = null){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_UNRESTRICTED_AUTH , TRUE);
		curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
		//如果存在data，则默认为post
		if($data){
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		}
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
	
	public function getCheckimg($url, $cookie){
		$img = $this->http_request($url, $cookie);
		return $img;
	}
	//模拟登录
	public function doLogin($url, $cookie, $data){
		$result = $this->http_request($url, $cookie, $data);
		return $result;
	}
	
	//封装返回格式
	public function returnRes(){
			
	}

	//返回高精度浮点随机数
	public function randomFloat($min = 0, $max = 1) {
    	return $min + mt_rand() / mt_getrandmax() * ($max - $min);
	}
}